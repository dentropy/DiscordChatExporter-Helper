# Troubleshooting Snippits

``` python
import export_guild
import json

with open('TheCoffeehouse.json') as json_file:
    server_channels = json.load(json_file)

export_guild.export_channel_to_file(export_guild.TOKEN, server_channels[5], export_guild.SERVER_NAME)

server_channels = export_guild.get_channels_IDs(export_guild.TOKEN, export_guild.GUILD_ID)
server_channels = export_guild.write_channel_IDs(server_channels)
```