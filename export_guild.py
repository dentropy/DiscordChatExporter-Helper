import subprocess
import json
from datetime import datetime
import os

import json
try:
    json_file = open("scrape.json")
except IOError:
    print("Please rename tmp.scrape.json to scrape.json and fill in the fields")
    exit()
finally:
    scraping_commands = json.load(json_file)

def get_channels_IDs(tmp_TOKEN, tmp_GUILD_ID):
    shell_command = '''docker run --rm -it tyrrrz/discordchatexporter:stable channels -t %s -g %s''' % (tmp_TOKEN, tmp_GUILD_ID)
    shell_output = subprocess.check_output(shell_command.split())
    shell_output = str(shell_output)[1:]
    tmp_shell_output = shell_output.split("\\r\\n")[:-1]
    server_channels = []
    for channel in tmp_shell_output:
        print(channel)
        channel_id = channel.split("|")[0]
        channel_type = channel.split("|")[1].split("/")[0][:-1]
        channel_name = channel.split("|")[1].split("/")[1][1:]
        server_channels.append({
            "channel": {
                "id": channel_id ,
                "type": channel_type ,
                "name": channel_name
            }
        })
    return server_channels

def write_channel_IDs(tmp_file_path, tmp_server_name, tmp_server_channels):
    if not os.path.isdir('%s/channel_ids/' % tmp_file_path):
        os.mkdir('%s/channel_ids/' % tmp_file_path)
    with open('%s/channel_ids/%s.json' % (tmp_file_path, "TheCoffeehouse"), 'w') as outfile:
        json.dump(tmp_server_channels, outfile)

def export_channel_to_file(tmp_TOKEN, tmp_server_name, channel_medata):
    # TODO Allow manually set Home Folder
    shell_command = '''docker run --rm -it \\
        -v /home/dentropy/Documents/Projects/DiscordChatExporter/%s/%s-%s:/app/out \\
        tyrrrz/discordchatexporter:stable \\
        export -t %s -c %s --dateformat "yyyy-MM-dd HH:mm:ss.ffff" \\
        -f Json --media --reuse-media -o messages.%s.json''' % (
            tmp_server_name,
            channel_medata["channel"]["name"],
            datetime.today().strftime('%Y-%m-%d'),
            tmp_TOKEN, 
            channel_medata["channel"]["id"],
            channel_medata["channel"]["name"],)
    print(shell_command)
    shell_output = subprocess.Popen(shell_command, stdout=subprocess.PIPE, shell=True)
    shell_output.wait()
    print("Finished exporting %s" % channel_medata["channel"]["name"])
    return True


server_channels = get_channels_IDs(scraping_commands["TOKEN"], scraping_commands["SERVERS"][0]["GUILD_ID"])
write_channel_IDs(
    scraping_commands["OUTPUT_PATH"],
    scraping_commands["SERVERS"][0]["SERVER_NAME"],
    server_channels
)

# You can optionally read a list of channel ID's already exported
#with open('/SOMEPATH/SERVERNAME.json') as json_file:
#    server_channels = json.load(json_file)

for channel in server_channels:
    export_channel_to_file(
        scraping_commands["TOKEN"], 
        scraping_commands["SERVERS"][0]["SERVER_NAME"], 
        channel
    )

print("Finished")