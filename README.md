# Guild Exporter

A script to help using [Tyrrrz/DiscordChatExporter: Exports Discord chat logs to a file](https://github.com/Tyrrrz/DiscordChatExporter) to export entire guilds. 

# Requirements:

* python3
* docker
* [Discord Token and Channel ID's](https://github.com/Tyrrrz/DiscordChatExporter/wiki/Obtaining-Token-and-Channel-IDs))

# Instructions:

1. Install Docker
2. ```docker pull tyrrrz/discordchatexporter:stable```
3. Rename tmp.scrape.json as scrape.json and fill in all the values for all keys.
  * Note that you can provide a list of guilds
4. ```python3 export_guild.py```

## Additional Documentation:

* [Docker usage instructions · Tyrrrz/DiscordChatExporter Wiki](https://github.com/Tyrrrz/DiscordChatExporter/wiki/Docker-usage-instructions)